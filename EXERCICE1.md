# Exercice 1: prise en main des données et du format fastq
---

1. Se rendre sur le site du [NCBI](https://www.ncbi.nlm.nih.gov/) et en une requête, rechercher les run SRR7062654 et SRR7062655.

  %accordion%Solution%accordion%

   - saisir SRR7062654 OR SRR7062655
   - puis aller dans SRA

  %/accordion%

2. Explorer les metadonnées : organisme ? échantillon ? séquenceur utilisé ? séquençage pairé ? 

  %accordion%Solution%accordion%

   - poulet 
   - Ethioian-Horro1-indigenous-chicken
   - Illumina HiSeq 2500
   - PAIRED

  %/accordion%

3. Manipulation du format fastq.  
  A partir des fichiers que vous avez recupérés comme indiqué sur la page introduction, faites les opérations suivantes.  
  * Afficher le contenu du fichier `SRR7062654_R1.fastq.gz` à l'aide de la commande `zmore`.
  
  %accordion%Solution%accordion%

   ```bash
  zmore SRR7062654_R1.fastq.gz
   ```

  %/accordion%
 
 * Extraire et comparer les 5 premiers identifiants de chaque fastq du run SRR7062654.
 
  %accordion%Solution%accordion%

   ```bash
  zgrep '^@SRR' SRR7062654_R1.fastq.gz | head -n 5
   ```
   ```
   @SRR7062654.369
   @SRR7062654.405
   @SRR7062654.484
   @SRR7062654.649
   @SRR7062654.706
   ```
   ```bash
   zgrep '^@SRR' SRR7062654_R2.fastq.gz | head -n 5
   ```
   ```
   @SRR7062654.369
   @SRR7062654.405
   @SRR7062654.484
   @SRR7062654.649
   @SRR7062654.706
   ```
 
 %/accordion%
 
 * Quel est le nombre de fragments dans le run SRR7062654 ?
 
 %accordion%Solution%accordion%

   ```bash
   zgrep -c '^@SRR' SRR7062654_R?.fastq.gz
   ```
   ```bash
   SRR7062654_R1.fastq.gz: 993954
   SRR7062654_R2.fastq.gz: 993954
   ```

  %/accordion%
  
 * Combien de lectures contiennent un ou plusieurs « N » dans le run SRR7062654 ?

   %accordion%Solution%accordion%

   ```bash
   zgrep -c '^[ATCG]*N[ATCGN]*$' SRR7062654_R?.fastq.gz
   ```
   ```bash
   SRR7062654_R1.fastq.gz:2887
   SRR7062654_R2.fastq.gz:2778
   ```

   %/accordion%

4. Analyse de la qualité du séquençage, pour cela suivre les indications suivantes :
   * Rechercher le module fastqc
   * Charger le module
   * Lancer le logiciel fastqc sur chaque fichier fastq (plusieurs façons de faire mais dans tous les cas l'exécution doit se faire sur le cluster !)
   * Vérifier l'exécution à l'aide de la commande `squeue -u USERNAME`

   %accordion%Solution oneline %accordion%

   ```bash
   search_module fastqc
   module load bioinfo/FastQC/0.12.1
   for i in *.fastq.gz ;  do echo "fastqc $i"; done > 0_fastqc.jobs
   mkdir LOGS
   sarray -J 0_fastqc -e LOGS/%x_%j.err -o LOGS/%x_%j.out 0_fastqc.jobs
   ```
   💡 la syntaxe de la boucle for en Bash est `for VAR in LIST; do CMDS; done`. La variable `VAR` prend tour à tour les valeurs contenues dans `LIST` et les commandes `CMDS` sont exécutées. Ici `LIST` est constituée des noms des fichiers du répertoire courant se terminant par `.fastq.gz`.

   %/accordion%

5. Explorer les rapports html en utilisant le public_html.

   * Créer un répertoire pour rendre accessibles les fichiers via le web :
   ```bash
    mkdir -p ~/save/public_html; ln -s ~/save/public_html ~/public_html
    chmod a+x ~/save ; chmod 755 ~/save/public_html
   ```
   * Copier les rapports fastQC `*.html` dans votre `~/public_html`
   ```bash
    cp *.html ~/public_html/
   ```
   Accéder via un navigateur à la page https://web-genobioinfo.toulouse.inrae.fr/~USERNAME et visualiser les fichiers html.

6. Fin de l'exercice.

   %accordion%Solution%accordion%

   ```
   Bingo: aller boire un café ! 
   ```

   %/accordion%
