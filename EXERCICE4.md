# Exercice 4 : visualisation - IGV
---

La visualisation se fait à l'aide du logiciel IGV sur votre ordinateur. Pour cela, il vous faut disposer des fichiers suivants :
* fasta & fai (index du fasta de la référence),
* bam & bai (index des fichiers bam d'alignements).

4. Télécharger les fichiers précédemment générés sur votre ordinateur :
    * récuperer les fichiers bam et bai avec le bouton de download de mobaXterm,
    * récuperer les fichiers du génome, le fichier fasta et le fai.

5. Si non-installé, aller télécharger IGV: http://www.broadinstitute.org/software/igv/download comme décrit dans la [page ressources](https://bios4biol.pages.mia.inra.fr/training-aln-variant-calling/ressources/prerequis.html#installation-de-igv) d'une autre formation.

6. Lancer IGV et charger les données (voir le mode d'emploi sur cette [page ressources](https://bios4biol.pages.mia.inra.fr/training-aln-variant-calling/ressources/IGV.html)).
    * Charger le génome (fichier fasta).
    * Charger les `*.bam`.
      
7. Explorer les résultats :  
    * Afficher les informations relatives à une lecture.

    %accordion%Solution%accordion%

      Clic sur une lecture : affichage des informations contenues dans le bam.

    %/accordion%

    * Quels sont les champs du format SAM qui ne sont pas affichés dans ce menu ?

    %accordion%Solution%accordion%

      Hidden tags: XA, MD, RG.

    %/accordion%

    * Afficher les lectures afin de visualiser les paires.

    %accordion%Solution%accordion%

      Clic droit sur la piste du bam et sélectionner « View as pairs ».

    %/accordion%

    * Afficher de nouveau les informations relatives à une lecture. Quel est l'avantage du mode « Vue par paire » ?

    %accordion%Solution%accordion%

      Le menu affiche les données des deux lectures de la paire.

    %/accordion%

    * Rendez-vous en position 25:13,657-15,407. Pourquoi certaines lectures ne sont-elles pas colorées ? A quoi cela est-il dû ?

    %accordion%Solution%accordion%

      Les lectures non colorées ont une MAPQ égale à zéro. Classiquement, cela est dû au fait que plusieurs localisations équiprobables ont été trouvées pour cette paire. Les autres localisations sont indiquées dans le tag XA qui n'est pas présenté dans le menu. Il faut donc rechercher les données de localisations alternatives dans le bam. Exemple :  
      `samtools view SRR7062654.bam | grep SRR7062654.79295892`

    %/accordion%

    * Quel est le type de subtitution à la position 25:5,825 ?

    %accordion%Solution%accordion%

      C/T

    %/accordion%

    * Trouver un SNP qui discrime les deux individus.

    %accordion%Solution%accordion%

      * 25:13,066
        * SRR7062654 C/C
        * SRR7062655 G/G
      
      * 25:21,156
        * SRR7062654 A/A
        * SRR7062655 C/C

    %/accordion%
