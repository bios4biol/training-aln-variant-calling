# Exercice 5 : préprocessing et calling - GATK
---

1. Charger les modules GATK (v4.4.0.0), Python (v3.11.1) et Java (v17.0.6).

 %accordion%Solution%accordion%

   ```bash
   search_module gatk
   module load bioinfo/GATK/4.4.0.0
   module load devel/python/Python-3.11.1 devel/java/17.0.6
   ```
 
 %/accordion%


1. Marquer les duplicats (GATK MarkDuplicates). 

 %accordion%Solution%accordion%

   Bash
   ```bash
   for bam in *.bam; do echo "gatk --java-options \"-Xms4000m -Xmx7g\" MarkDuplicates --INPUT $bam --METRICS_FILE ${bam}.metrics --TMP_DIR . --ASSUME_SORT_ORDER coordinate --CREATE_INDEX true --OUTPUT ${bam%.bam}.md.bam"; done > 4_md.jobs
   ```

   Perl
   ```bash
   \ls *.bam | perl -lne '$out=$_; $out=~s/\.bam//; print "gatk --java-options \"-Xms4000m -Xmx7g\" MarkDuplicates --INPUT $_ --METRICS_FILE $_.metrics --TMP_DIR . --ASSUME_SORT_ORDER coordinate --CREATE_INDEX true --OUTPUT $out.md.bam"' > 4_md.jobs
   ```

   ```bash
   sarray -J 4_md -e LOGS/%x_%j.err -o LOGS/%x_%j.out --mem=10G 4_md.jobs
   ```
 
 %/accordion%

1. Combien de lecture sont dupliquées dans le run SRR7062654 ?

 %accordion%Solution%accordion%

   ```bash
   grep ^LIBRARY -A1 SRR7062654.bam.metrics | datamash transpose
   ```
   ```bash
   UNPAIRED_READ_DUPLICATES: 278
   READ_PAIR_DUPLICATES: 27580
   ```
   ```bash
   samtools view -f 1024 SRR7062654.md.bam | wc -l
   55438 = 27580x2 + 278
   ```
 
 %/accordion%
1. Recalibrer les bases (BQSR), étape 1 GATK BaseRecalibrator.

 %accordion%Solution%accordion%

   Bash
   ```bash
   for bam in *.md.bam; do echo "gatk --java-options -Xmx7g BaseRecalibrator -I $bam -O ${bam%.bam}.recal.table --tmp-dir . -R Gallus_gallus.Gallus_gallus-5.0.dna.toplevel_chr25-26.fa --known-sites Gallus_gallus_incl_consequences_chr25-26.vcf --verbosity INFO"; done > 5_recal1.jobs
   ```

   Perl
   ```bash
   \ls *.md.bam | perl -lne '$out=$_; $out=~s/\.bam//; print "gatk --java-options -Xmx7g BaseRecalibrator -I $_ -O $out.recal.table --tmp-dir . -R Gallus_gallus.Gallus_gallus-5.0.dna.toplevel_chr25-26.fa --known-sites Gallus_gallus_incl_consequences_chr25-26.vcf --verbosity INFO"' > 5_recal1.jobs
   ```

   ```bash
   sarray -J 5_recal1 -e LOGS/%x_%j.err -o LOGS/%x_%j.out --mem=10G 5_recal1.jobs
   ```
 
 * Erreur1 : remarque il faut l'index (.fai) de la référence. 
    ```bash
    samtools faidx Gallus_gallus.Gallus_gallus-5.0.dna.toplevel_chr25-26.fa
    ```
 * Erreur2 : remarque il faut le dictionaire (.dict) de la référence. 
    ```bash
    gatk --java-options "-Xms4000m -Xmx7g" CreateSequenceDictionary -R Gallus_gallus.Gallus_gallus-5.0.dna.toplevel_chr25-26.fa
    ```
 
 %/accordion%
1. Recalibrer les bases (BQSR), étape 2 GATK ApplyBQSR.

 %accordion%Solution%accordion%

   Bash
   ```bash
   for bam in *.md.bam; do echo "gatk --java-options -Xmx14g ApplyBQSR -R Gallus_gallus.Gallus_gallus-5.0.dna.toplevel_chr25-26.fa --input $bam --output ${bam%.bam}.recal.bam --bqsr-recal-file ${bam%.bam}.recal.table"; done > 6_recal2.jobs
   ```

   Perl
   ```bash
   \ls *.md.bam | perl -lne '$out=$_; $out=~s/\.bam//; print "gatk --java-options -Xmx14g ApplyBQSR -R Gallus_gallus.Gallus_gallus-5.0.dna.toplevel_chr25-26.fa --input $_ --output $out.recal.bam --bqsr-recal-file $out.recal.table"' > 6_recal2.jobs
   ```

   ```bash
   sarray -J 6_recal2 -e LOGS/%x_%j.err -o LOGS/%x_%j.out --mem=15G 6_recal2.jobs
   ```
 
 %/accordion%
1. A l'aide de samtools stats comparer avant / après recalibration.

 %accordion%Solution%accordion%

   ```bash
   samtools stat SRR7062654.bam > SRR7062654.bam.stat
   /usr/local/bioinfo/src/samtools/samtools-1.18/misc/plot-bamstats SRR7062654.bam.stat -p SRR7062654.bam.stat
   samtools stat SRR7062654.md.recal.bam > SRR7062654.md.recal.bam.stat
   /usr/local/bioinfo/src/samtools/samtools-1.18/misc/plot-bamstats SRR7062654.md.recal.bam.stat -p SRR7062654.md.recal.bam.stat

   mkdir ~/public_html/SRR7062654.bam.stat
   cp SRR7062654.bam.stat* ~/public_html/SRR7062654.bam.stat
   mkdir ~/public_html/SRR7062654.md.recal.bam.stat
   cp SRR7062654.md.recal.bam.stat* ~/public_html/SRR7062654.md.recal.bam.stat
   ```
 
 %/accordion%
1. Lancer le calling pour produire un G.VCF par échantillon (GATK HaplotypeCaller).

 %accordion%Solution%accordion%

   Bash
   ```bash
   for bam in *.md.recal.bam; do echo "gatk --java-options \"-Xmx10g -Xms1000m\" HaplotypeCaller -R Gallus_gallus.Gallus_gallus-5.0.dna.toplevel_chr25-26.fa -I $bam -O ${bam%.bam}.g.vcf -ERC GVCF"; done > 7_HaplotypeCaller.jobs
   ```

   Perl
   ```bash
   \ls *.md.recal.bam | perl -lne '$out=$_; $out=~s/\.bam//; print "gatk --java-options \"-Xmx10g -Xms1000m\" HaplotypeCaller -R Gallus_gallus.Gallus_gallus-5.0.dna.toplevel_chr25-26.fa -I $_ -O $out.g.vcf -ERC GVCF"' > 7_HaplotypeCaller.jobs
   ```

   ```bash
   sarray -J 7_haplotypecaller -e LOGS/%x_%j.err -o LOGS/%x_%j.out --mem=15G 7_HaplotypeCaller.jobs
   ```
 
 %/accordion%
1. A partir des G.VCF individuels créer un G.VCF multisample. 

 %accordion%Solution%accordion%

   Bash
   
   ```bash
   cmd="gatk --java-options -Xmx10g CombineGVCFs -R Gallus_gallus.Gallus_gallus-5.0.dna.toplevel_chr25-26.fa -O SRR.g.vcf"
   for vcf in *.g.vcf; do cmd+=" --variant $vcf"; done
   echo $cmd > 8_CombineGVCFs.jobs
   ```
   💡 l'opérateur `+=` est un raccourci de `cmd=cmd+"  --variant $vcf"`

   Perl
   ```bash
   \ls *.g.vcf | perl -ne 'BEGIN { print "gatk --java-options -Xmx10g CombineGVCFs -R Gallus_gallus.Gallus_gallus-5.0.dna.toplevel_chr25-26.fa -O SRR.g.vcf"} chomp(); print " --variant $_"; END{print "\n"}' > 8_CombineGVCFs.jobs
   ```
   
   ```bash
   sarray -J 8_CombineGVCFs -e LOGS/%x_%j.err -o LOGS/%x_%j.out --mem=15G 8_CombineGVCFs.jobs
   ```
 
 %/accordion%
1. Faire le Calling GATK GenotypeGVCFs.

 %accordion%Solution%accordion%

   ```bash
   cat > 9_GenotypeGVCFs.jobs
   gatk --java-options "-Xmx10g" GenotypeGVCFs -R Gallus_gallus.Gallus_gallus-5.0.dna.toplevel_chr25-26.fa -V SRR.g.vcf -O SRR.vcf.gz
   ^C
   sarray -J 9_GenotypeGVCFs -e LOGS/%x_%j.err -o LOGS/%x_%j.out --mem=15G 9_GenotypeGVCFs.jobs
   ```
 
 %/accordion%
