# Excercice 6 : format VCF annotation et filtre
---

1. Repérer les différents champs du fichier VCF obtenu précédemment.

 %accordion%Solution%accordion%

   ```bash
   zmore SRR.vcf.gz
   ``` 

 %/accordion%
 
1. Combien de variants contient le fichier VCF ?

 %accordion%Solution%accordion%

   ```bash
   zgrep -cv "^#" SRR.vcf.gz
   ```
   ```
   82386
   ```
 
 %/accordion%

1. Charger le module de SNPeff (v4.3T) et afficher l'aide de la commande

 %accordion%Solution%accordion%

   ```bash
   search_module snpeff
   module load bioinfo/SnpEff/4.3T
   snpEff -h
   ```
 
 %/accordion%

1. Une annotation est-elle disponible pour notre génome d'intérêt ? Si oui, laquelle ?

 %accordion%Solution%accordion%

   ```bash
   snpEff databases | grep -i gallus
   ```
   ```
   Gallus_gallus-5.0.86
   ```
 
 %/accordion%

1. Lancer l'annotation et repérer les ajouts dans le VCF (champ INFO)

 %accordion%Solution%accordion%

   ```bash
   snpEff ann Gallus_gallus-5.0.86 SRR.vcf.gz -s SRR-snpEff_summary.html > SRR-snpEff.vcf ; bgzip SRR-snpEff.vcf ; tabix -p vcf SRR-snpEff.vcf.gz
   ```
 
 %/accordion%

1. Afficher l'aide de la commande SNPsift

 %accordion%Solution%accordion%

   ```bash
   java -Xmx4G -jar /usr/local/bioinfo/src/SnpEff/SnpEff-4.3T/snpEff/SnpSift.jar
   ```
 
 %/accordion%

1. Ajouter l'information « snp connus » dans le champ ID du VCF

 %accordion%Solution%accordion%

   ```bash
   java -Xmx4G -jar /usr/local/bioinfo/src/SnpEff/SnpEff-4.3T/snpEff/SnpSift.jar annotate Gallus_gallus_incl_consequences_chr25-26.vcf SRR-snpEff.vcf.gz > SRR-snpEff-annotate.vcf ; bgzip SRR-snpEff-annotate.vcf ; tabix -p vcf SRR-snpEff-annotate.vcf.gz
   ```
 
 %/accordion%

1. Combien de "nouveaux" variants dans le VCF

 %accordion%Solution%accordion%

   ```bash
   zgrep -v "^##" SRR-snpEff-annotate.vcf.gz | cut -f3 | grep -c '\.' 
   ```
   ```
   20146
   ```
 
 %/accordion%

1. Filtre du vcf
 * Combien de variants ont une qualité >= 100
 * Combien de nouveaux SNP de qualité >= 200 sont homozygotes références pour SRR7062654 et homozygotes alternatifs pour SRR7062655 ?
 * Trouver la position du SNP nouveau ayant un effet "HIGH" et la qualité maximum

 %accordion%Solution%accordion%

   ```bash
   java -Xmx4G -jar /usr/local/bioinfo/src/SnpEff/SnpEff-4.3T/snpEff/SnpSift.jar filter "QUAL>=100" -f SRR-snpEff-annotate.vcf.gz | grep -vc '^#'
   ```
   ```
   79213
   ```
   ```bash
   java -Xmx4G -jar /usr/local/bioinfo/src/SnpEff/SnpEff-4.3T/snpEff/SnpSift.jar filter "ID == '' & isHom(GEN[SRR7062654]) & isHom(GEN[SRR7062655]) & isRef(GEN[SRR7062654]) & isVariant(GEN[SRR7062655]) & (REF=~'^[ATGC]$') & (ALT=~'^[ATGC]$') & (DP>20) & QUAL>=200" -f SRR-snpEff-annotate.vcf.gz | grep -cv '^#'
   ```
   ```
   445
   ```
   ```bash
   java -jar -Xmx4G /usr/local/bioinfo/src/SnpEff/SnpEff-4.3T/snpEff/SnpSift.jar filter "(ANN[*].IMPACT='HIGH') & (REF=~'^[ATGC]$') & (ALT=~'^[ATGC]$')" -f SRR-snpEff-annotate.vcf.gz | grep -v "^#"  |cut -f1-6 |sort -n -k6,6
   ```
   ```
    25	1753972	.	T	G	965.5
   ```
 
 %/accordion%
