# Short-read alignment and small size variants calling
![image](img/logos.png)

---
MAJ:20/10/2023

### Objectifs:

Cette formation a pour but de vous aider à traiter les séquences courtes issues des plates-formes de séquençage Illumina. Vous y découvrirez les formats de séquences et d’alignement les biais connus et mettrez en œuvre des logiciels d'alignement sur génome de référence, la recherche de variants, l'annotation de variants et l'analyse a l'aide du pipeline nf-core/Sarek.


__Pré-requis: savoir utiliser un environnement Unix.__

Pour réaliser l'ensemble de ces exercices, connectez-vous sur votre compte `genobioinfo` en utilisant `mobaXterm` depuis un poste windows ([voir les instructions de connexion](ressources/mobaxterm.md)) ou la commande `ssh` depuis un poste linux.
 ```bash
 ssh -X <username>@genobioinfo.toulouse.inrae.fr
 ```
Pour les traitements « lourds » utilisez le cluster avec la commande `srun --pty bash` ou `srun --x11 --pty bash` (pour l’interface graphique).

Pour sélectionner les lignes de commande que vous souhaitez copier-coller dans votre terminal, cliquez 3 fois sur la ligne. Ceci vous permettra de facilement sélectionner entièrement la ligne.

## Genotoul cluster reminder.

{% pdf title="genotoul cluster", src="doc/Revision Cluster.pdf", width="100%", height="550", link=true %}{% endpdf %}


### Mise en place de l'environnement de travail:

1. Se connecter sur le serveur `genobioinfo.toulouse.inrae.fr` suivre les [indications de la page ressources -> Connexion SSH avec mobaXterm](ressources/mobaxterm.md)
2. Positionnement dans le répertoire de travail accessible en écriture depuis le cluster
 ```bash
 cd ~/work
 ```

3. Créer dans votre répertoire `work`, un répertoire de travail: `Formation_Aln-SNP`.

 ```bash
 mkdir Formation_Aln-SNP 
 cd Formation_Aln-SNP
 ``` 

4. Les données sur lesquelles nous allons travailler sont dans le répertoire `/save/user/formation/public_html/16_SGS-SNP/Data_Chr25-26/`
Créer des liens sur les fichiers a l'aide des commandes suivantes:
 ```bash
 ln -s /save/user/formation/public_html/16_SGS-SNP/Data_Chr25-26/G* .
 ln -s /save/user/formation/public_html/16_SGS-SNP/Data_Chr25-26/S* .
 ```

5. Verifier que les 7 fichiers sont bien présents.
