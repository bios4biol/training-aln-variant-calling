# Summary

* [Introduction](README.md)
* [Exercice1: Qualité](EXERCICE1.md)
* [Exercice2: Alignement](EXERCICE2.md)
* [Exercice3: SAM/BAM](EXERCICE3.md)
* [Exercice4: Visualisation](EXERCICE4.md)
* [Exercice5: GATK](EXERCICE5.md)
* [Exercice6: Annotation](EXERCICE6.md)
* [Exercice7: Nextflow](NEXTFLOW.md)
* Ressources
 * [Installation des pré-requis](ressources/prerequis.md)
 * [MobaXterm: Connexion SSH](ressources/mobaxterm.md)
 * [Visualisation avec IGV](ressources/IGV.md)
 
