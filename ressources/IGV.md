# Visualiser vos bam avec IGV
---

1. Lancer IGV

2. Charger un génome (fichier fasta)<br>
![image](img/IGV_1_Loadgenome.png)

3. Charger une annotation (fichier gtf ou gff)<br>
![image](img/IGV_2_loadgtf.png)
 > Noter que l'on peut faire une recherche par mots-clés ou par coordonnées.

4. Charger les alignements (fichier bam, avec un fichier bai à côté)<br>
![image](img/IGV_3_loadbam.png)

5. En général la vue est trop large pour voir les alignements il est nécessaire de zoomer.<br>
![image](img/IGV_4_zoom.png)

Pour comprendre la navigation dans l'interface vous pouvez visualiser les vidéos suivantes : 

 * pour les données de séquence :  https://www.youtube.com/watch?v=E_G8z_2gTYM
 * pour les données de RNAseq : https://www.youtube.com/watch?v=awGN-rpLYas
 * plus généraliste, base de IGV : https://www.youtube.com/watch?v=YpNg0hNUuo8
