# Connexion SSH avec mobaXterm
---

1. Lancer mobaXterm et sélectionner "new session"<br>
 ![image](img/moba1_new_session.png)

2. Indiquer le hostname (__genobioinfo.toulouse.inrae.fr__) et username. Pour information vous pouvez voir que par défaut le __X11 forwarding__ est activé (pour avoir les fenêtres graphiques).<br>
 ![image](img/moba2_config_session.png)

3. Une fois la session activée, vous obtenez un onglet avec la session active.<br>
 ![image](img/moba3_session_open.png)

# Transfert de fichiers avec mobaXterm
--- 

Vous pourrez réaliser du transfert de fichiers directement dans mobaXterm avec l'onglet __SFTP__ à gauche.
 - Les boutons d'upload et de download vous permettront l'échange de fichiers.
 - Le gliser/déposer fonctionne également.<br>
 ![image](img/moba4_transfert.png)
