# Pré-requis pour la formation en distantiel
---

Cette page contient les instructions d'installation pour les logiciels suivant :
* [Installation MobaXterm](#installation-de-mobaxterm)
* [Installation IGV](#installation-de-igv)

Pour l'utilisation veuillez vous reporter aux les pages suivantes :
* [Utilisation de mobaXterm](./mobaxterm.md)
* [Visualisation avec IGV](./IGV.md)

# Installation de mobaXterm

Télécharger et installer la portable edition a cette url :

* https://mobaxterm.mobatek.net/download-home-edition.html


# Installation de IGV

1. Télécharger l'executable à l'adresse suivante: https://software.broadinstitute.org/software/igv/download.
Si vous ne savez pas quelle version de Java vous avez, prenez directement la version incluant Java.
![image](img/IGV_download.png)
2. Lancer l'installeur et accepter la licence.<br>
![image](img/IGV_installer.png)


